/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicioalgotitmos;

/**
 *
 * @author FREDDY
 */
//Freddy Sebastian Chia Vera - Codigo: 1151494
// Anderson Augusto Navarro Baron - Codigo: 1151482
import java.util.Scanner;
import javax.swing.JOptionPane;
public class FizzBuzz {
    
    
   	public static void main(String args[]){
            int i;
            i = Integer.parseInt(JOptionPane.showInputDialog("Introduzca el Dato"));
            
        for (int x = 1; x <= i; x++){
            if ((x % 3 == 0) && (x % 5 == 0)){
                System.out.println(x+" = FizzBuzz");
            } else if(x % 3 == 0){
                System.out.println(x+" = Fizz");
            } else if(x % 5 == 0){
                System.out.println(x+" = Buzz");
            } else {
                System.out.println(x);
            }
        }
    }
}
